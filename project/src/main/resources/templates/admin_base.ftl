<#macro page_head>
    <title>W Orchestra</title>
</#macro>

<#macro content>
    <#--nothing-->
</#macro>

<#macro js_custom>
<#--nothing-->
</#macro>

<#macro main_template>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="description" content="Miminium Admin Template v.1">
    <meta name="author" content="Isna Nur Azis">
    <meta name="keyword" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <@page_head/>

    <!-- start: Css -->
    <link rel="stylesheet" type="text/css" href="/asset/css/bootstrap.min.css">

    <!-- plugins -->
    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/simple-line-icons.css"/>
    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/mediaelementplayer.css"/>
    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/animate.min.css"/>
    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/icheck/skins/flat/red.css"/>
    <link href="/asset/css/style.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/animate.min.css"/>
    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/nouislider.min.css"/>
    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/ionrangeslider/ion.rangeSlider.css"/>
    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/ionrangeslider/ion.rangeSlider.skinFlat.css"/>
    <link rel="stylesheet" type="text/css" href="/asset/css/plugins/bootstrap-material-datetimepicker.css"/>
    <!-- end: Css -->

    <link rel="shortcut icon" href="/asset/img/logomi.png">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="mimin" class="dashboard">
<!-- start: Header -->
<nav class="navbar navbar-default header navbar-fixed-top">
    <div class="col-md-12 nav-wrapper">
        <div class="navbar-header" style="width:100%;">
            <div class="opener-left-menu is-open">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
            </div>
            <a href="/" class="navbar-brand">
                <b>WHIPLASH ORCHESTRA</b>
            </a>


            <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span>ADMIN</span></li>
                <li class="dropdown avatar-dropdown">
                    <img src="/asset/img/avatar.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- end: Header -->

<div class="container-fluid mimin-wrapper">

    <!-- start:Left Menu -->
    <div id="left-menu">
        <div class="sub-left-menu scroll">
            <ul class="nav nav-list">
                <li><div class="left-bg"></div></li>
                <li class="time">
                    <h1 class="animated fadeInLeft">21:00</h1>
                    <p class="animated fadeInRight">Sat,October 1st 2029</p>
                </li>


                <#--<li class="ripple"><a href="/participant/profile"><span class="fa fa-calendar-o"></span>Profile</a></li>-->
                <li class="ripple"><a href="/admin/users"><span class="fa fa-calendar-o"></span>Users</a></li>
                <li class="ripple"><a href="/admin/instruments"><span class="fa fa-calendar-o"></span>Instruments</a></li>
                <li class="ripple"><a href="/admin/documents"><span class="fa fa-calendar-o"></span>Documents</a></li>
                <li class="ripple"><a href="/admin/questions"><span class="fa fa-calendar-o"></span>Questions</a></li>
            </ul>
        </div>
    </div>
    <!-- end: Left Menu -->


    <!-- start: Content -->
    <@content/>
    <!-- end: content -->



    <!-- start: right menu -->

    <!-- end: right menu -->

</div>



<!-- st/art: Javascript -->
<script src="/asset/js/jquery.min.js"></script>
<script src="/asset/js/jquery.ui.min.js"></script>
<script src="/asset/js/bootstrap.min.js"></script>


<!-- plugins -->
<script src="/asset/js/plugins/icheck.min.js"></script>
<script src="/asset/js/plugins/moment.min.js"></script>
<script src="/asset/js/plugins/mediaelement-and-player.min.js"></script>
<script src="/asset/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="/asset/js/main.js"></script>
<@js_custom/>
<script type="text/javascript">
    $(document).ready(function(){
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
        });
        $('video,audio').mediaelementplayer({
            alwaysShowControls: true,
            videoVolume: 'vertical',
            features: ['playpause','progress','volume','fullscreen']
        });
    });
</script>
<!-- end: Javascript -->
</body>
</html>
</#macro>