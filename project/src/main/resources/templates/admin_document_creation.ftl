<#include "admin_base.ftl">

<#macro page_head>
    <title>W Orchestra</title>
</#macro>

<#macro content>


<div id="content">

    <form action="/admin/document/create" method="post" enctype="multipart/form-data">
        <div class="form-element">
            <div class="col-md-12 padding-0">
                <div class="col-md-8">
                    <div class="panel form-element-padding">
                        <div class="panel-heading">
                            <h4>Create Document</h4>
                        </div>
                        <div class="panel-body" style="padding-bottom:30px;">
                            <div class="col-md-12">
                                <div class="form-group"><label class="col-sm-2 control-label text-right">
                                    File
                                    </label>
                                    <div class="col-sm-10"><input type="file"
                                                                  class="form-control" name="file"
                                    ></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input class="submit btn btn-danger" type="submit" value="Create">
        </div>
    </form>
</div>

</#macro>

<#macro js_custom>
</#macro>

<@main_template/>