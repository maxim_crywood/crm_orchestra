<#include "manager_base.ftl">

<#macro page_head>
    <title>Job Applications</title>
</#macro>

<#macro content>
<div id="content">
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading"><h3>Questions</h3></div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Question</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list model.questions as question>
                                    <tr onclick="window.location='/manager/concrete_question?id=${question.id}';  ">
                                        <td>${question.content}</td>
                                        <td>${question.date}</td>
                                    </tr>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<#macro js_custom>
</#macro>

<@main_template/>