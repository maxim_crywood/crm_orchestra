<#include "participant_base.ftl">

<#macro page_head>
    <title>JobApp</title>
</#macro>

<#macro content>
<div id="content">
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading"><h3>Application</h3></div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%"
                               cellspacing="0">

                            <tbody>
                            <tr>
                                <td>
                                    Желаемая зарплата :
                                </td>
                                <td>
                                    ${model.jobapp.getWantedSalary()}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Инструмент :
                                </td>
                                <td>
                                    ${model.jobapp.getInstrument().getName()}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Текст Заявки :
                                </td>
                                <td>
                                    ${model.jobapp.getDescription()}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Статус :
                                </td>
                                <td>
                                    ${model.jobapp.getStatus()}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="/download_cv?jobappId=${model.jobapp.id}&id=${model.jobapp.document.id}">CV</a>
                                </td>
                                <td>
                                    <a href="/download_cv?jobappId=${model.jobapp.id}&id=${model.jobapp.document.id}">CV</a>
                                </td>
                            </tr>
    <#if model.sobes && model.jobapp.interviewDate != null>
        <tr>
            <td>
                Дата собеседования :
            </td>
            <td>
                ${model.jobapp.interviewDate}
            </td>
        </tr>
    </#if>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<#macro js_custom>
</#macro>

<@main_template/>