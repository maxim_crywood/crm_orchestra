<#include "admin_base.ftl">

<#macro page_head>
    <title>W Orchestra</title>
</#macro>

<#macro content>


<div id="content">

    <form action="/admin/question/create" method="post">
        <div class="form-element">
            <div class="col-md-12 padding-0">
                <div class="col-md-8">
                    <div class="panel form-element-padding">
                        <div class="panel-heading">
                            <h4>Create Question</h4>
                        </div>
                        <div class="panel-body" style="padding-bottom:30px;">
                            <div class="col-md-12">
                                <div class="form-group"><label class="col-sm-2 control-label text-right">
                                    Authors Login
                                    </label>
                                    <div class="col-sm-10"><input type="text"
                                                                  class="form-control" name="login"
                                    ></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group"><label
                                        class="col-sm-2 control-label text-right">Question</label>
                                    <div class="col-sm-10"><input type="text"
                                                                  class="form-control" name="content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input class="submit btn btn-danger" type="submit" value="Create">
        </div>
    </form>
</div>

</#macro>

<#macro js_custom>
</#macro>

<@main_template/>