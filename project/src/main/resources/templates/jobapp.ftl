<#include "participant_base.ftl">

<#macro page_head>
    <title>Make JobApp</title>
</#macro>

<#macro content>
<div id="content">
        <form action="/participant/add_jobapp" method="post" enctype="multipart/form-data">
            <div class="form-element">
                <div class="col-md-12 padding-0">
                    <div class="col-md-8">
                        <div class="panel form-element-padding">
                            <div class="panel-heading">
                                <h4>Job Application</h4>
                            </div>
                            <div class="panel-body" style="padding-bottom:30px;">
                                <div class="col-md-12">
                                    <div class="form-group"><label class="col-sm-2 control-label text-right">Wanted
                                        Salary</label>
                                        <div class="col-sm-10"><input type="number"
                                                                      class="form-control" name="wantedSalary"
                                        ></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group"><label
                                            class="col-sm-2 control-label text-right">Email</label>
                                        <div class="col-sm-10"><input type="email"
                                                                      class="form-control" name="email"
                                                                      value="${model.user.email}"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group"><label
                                            class="col-sm-2 control-label text-right">why</label>
                                        <div class="col-sm-10"><input type="text"
                                                                      class="form-control" name="description"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group"><label
                                            class="col-sm-2 control-label text-right">Choose Instrument</label>
                                        <div class="col-sm-10">
                                            <input list="instruments" name="instrumentName" id="instrumentName">
                                            <datalist id="instruments">
                                        <#list model.instruments as instrument>
                                            <option type="number" value="${instrument.name}">
                                        </#list>
                                            </datalist>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group"><label
                                            class="col-sm-2 control-label text-right">CV</label>
                                        <div class="col-sm-10"><input type="file"
                                                                      class="form-control" name="file"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input class="submit btn btn-danger" type="submit" value="Submit">
            </div>
        </form>
</div>
</#macro>

<#macro js_custom>
</#macro>

<@main_template/>