<#include "participant_base.ftl">

<#macro page_head>
    <title>Job Applications</title>
</#macro>

<#macro content>
<div id="content">
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading"><h3>My Applications</h3></div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Instrument</th>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list model.jobapps as jobapp>
                                    <tr onclick="window.location='/participant/concrete_jobapp?id=${jobapp.id}';  ">
                                        <td>${jobapp.instrument.name}</td>
                                        <td>${jobapp.date}</td>
                                        <td>${jobapp.status}</td>
                                    </tr>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<#macro js_custom>
</#macro>

<@main_template/>