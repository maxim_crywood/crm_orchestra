<#include "manager_base.ftl">

<#macro page_head>
    <title>Job Applications</title>
</#macro>

<#macro content>
<div id="content">

    <form action="/manager/all_jobapps" method="get">
        <div class="form-element">
            <div class="col-md-12 padding-0">
                <div class="col-md-8">
                    <div class="panel form-element-padding">
                        <div class="col-md-12">
                            <div class="form-group"><label
                                    class="col-sm-2 control-label text-right">Filter Status</label>
                                <div class="col-sm-10">
                                    <input list="statuses" name="status">
                                    <datalist id="statuses">
                                        <#list model.statuses as status>
                                            <option type="number" value="${status}">
                                        </#list>
                                    </datalist>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input class="submit btn btn-danger" type="submit" value="Filter">
        </div>
    </form>

    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading"><h3>Applications</h3></div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Instrument</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Author</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list model.jobapps as jobapp>
                                    <tr onclick="window.location='/manager/concrete_jobapp?id=${jobapp.id}';  ">
                                        <td>${jobapp.instrument.name}</td>
                                        <td>${jobapp.date}</td>
                                        <td>${jobapp.status}</td>
                                        <td>${jobapp.author.user.login}</td>
                                    </tr>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<#macro js_custom>
</#macro>

<@main_template/>