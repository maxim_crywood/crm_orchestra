<#include "manager_base.ftl">

<#macro page_head>
    <title>JobApp</title>
</#macro>

<#macro content>
<div id="content">
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading"><h3>Application</h3></div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%"
                               cellspacing="0">

                            <tbody>
                            <tr>
                                <td>
                                    Вопрос :
                                </td>
                                <td>
                                    ${model.question.content}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Ответ :
                                </td>
                                <td>
                                    <#if model.rotebal>
                                        ${model.question.answer}
                                    <#else>
                                        ожидайте)
                                    </#if>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Дата :
                                </td>
                                <td>
                                    ${model.question.date}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Ответить :
                                </td>
                                <td>
                                    <form method="post" action="/manager/answer">
                                        <input type="hidden" name="id" value="${model.question.id}">
                                        <input list="text" name="answer">
                                    </form>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<#macro js_custom>
</#macro>

<@main_template/>