<#include "admin_base.ftl">

<#macro page_head>
    <title>W Orchestra</title>
</#macro>

<#macro content>
<div id="content">
    <div class="col-md-12 top-20 padding-0">
        <a href="/admin/document/creation">CREATE DOCUMENT</a>
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading"><h3>Documents</h3></div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list model.documents as document>
                            <tr>
                                <td>${document.id}</td>
                                <td><a href="/admin/document?id=${document.id}">Edit</a></td>
                                <td><form action="/admin/document/delete" method="post">
                                    <input type="hidden" name="id" value="${document.id}">
                                    <input type="submit" value="Delete">
                                </form></td>
                            </tr>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<#macro js_custom>
</#macro>

<@main_template/>