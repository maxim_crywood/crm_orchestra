<#include "admin_base.ftl">

<#macro page_head>
    <title>W Orchestra</title>
</#macro>

<#macro content>
<div id="content">
    <div class="col-md-12 top-20 padding-0">
        <a href="/admin/user/creation">CREATE USER</a>
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading"><h3>Users</h3></div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Login</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list model.users as user>
                            <tr>
                                <td>${user.login}</td>
                                <td>${user.email}</td>
                                <td>${user.role}</td>
                                <td><a href="/admin/user?id=${user.id}">Edit</a></td>
                                <td><form action="/admin/user/delete" method="post">
                                    <input type="hidden" name="id" value="${user.id}">
                                    <input type="submit" value="Delete">
                                </form></td>
                            </tr>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<#macro js_custom>
</#macro>

<@main_template/>