<#include "admin_base.ftl">

<#macro page_head>
    <title>W Orchestra</title>
</#macro>

<#macro content>
<div id="content">
    <div class="col-md-12 top-20 padding-0">
        <a href="/admin/question/creation">CREATE QUESTION</a>
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading"><h3>Questions</h3></div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Author</th>
                                <th>Question</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list model.questions as question>
                            <tr>
                                <td>${question.author.user  .login}</td>
                                <td>${question.content}</td>
                                <td><a href="/admin/question?id=${question.id}">Edit</a></td>
                                <td><form action="/admin/question/delete" method="post">
                                    <input type="hidden" name="id" value="${question.id}">
                                    <input type="submit" value="Delete">
                                </form></td>
                            </tr>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>

<#macro js_custom>
</#macro>

<@main_template/>