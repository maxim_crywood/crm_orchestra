<html>
<body>
<form method="post" action="/participant/add_message">
    <input type="text" name="text" id="text" placeholder="your message">
    <input type="hidden" name="conversationId" id="conversationId" value="${model.conversation.id}">
    <input type="submit">
</form>
</br>
<table>
    <#list model.conversation.messages as message>
        <tr>
            <td>
                <h1>${message.author.user.login} :</h1>
            </td>
            <td>
                ${message.text}
            </td>
        </tr>
    </#list>
</table>
</body>
</html>