package ru.kpfu.itis.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.app.model.Conversation;
import ru.kpfu.itis.app.model.Message;

import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface MessageRepository extends JpaRepository<Message, Long> {
    Optional<Message> findOneById(Long id);
    List<Message> findAllByConversation(Conversation conversation);
}
