package ru.kpfu.itis.app.services.impl;

import org.hibernate.stat.QueryStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.forms.QuestionCreateForm;
import ru.kpfu.itis.app.forms.QuestionEditForm;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.Question;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.ParticipantRepository;
import ru.kpfu.itis.app.repositories.QuestionRepository;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.services.QuestionService;

import java.util.*;

/**
 * Maxim Yagafarov
 **/
@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private ParticipantRepository participantRepository;

    @Override
    public Optional<Question> findOneById(Long id) {
        return questionRepository.findOneById(id);
    }

    @Override
    public List<Question> findAllByAuthor(ParticipantData author) {
        List<Question> ret = questionRepository.findAllByAuthor(author);
        sortByDate(ret, true);
        return ret;
    }

    private void sortByDate(List<Question> ret, boolean inv) {
        Collections.sort(ret, Comparator.comparing(Question::getDate));
        if (inv) Collections.reverse(ret);
    }

    @Override
    public List<Question> findAll() {
        List<Question> ret = questionRepository.findAll();
        sortByDate(ret, true);
        return ret;
    }

    @Override
    public Question addQuestion(String content, ParticipantData user) {
        Question question = Question.builder()
                .author(user)
                .date(new Date(System.currentTimeMillis()))
                .content(content)
                .answered(false)
                .build();
        System.out.println("question = " + question);
        return questionRepository.save(question);
    }

    @Override
    public Question answer(Question question, String answer, User user) {
        question.setAnswer(answer);
        question.setAnswered(true);
        question.setManager(user);
        return questionRepository.save(question);
    }

    @Override
    public Question create(QuestionCreateForm form) {
        return addQuestion(form.getContent(), participantRepository.findOneByUser(
                usersRepository.findOneByLogin(form.getLogin()).get()).get());
    }

    @Override
    public Question updateInfo(QuestionEditForm form) {
        Optional<Question> opt = questionRepository.findOneById(form.getId());
        if (!opt.isPresent()) return null;
        Question question = opt.get();
        question.setContent(form.getContent());
        question.setAuthor(participantRepository.findOneByUser(
                usersRepository.findOneByLogin(form.getLogin()).get()
        ).get());
        return questionRepository.save(question);
    }

    @Override
    public void deleteQuestion(Long id) {
        questionRepository.delete(id);
    }
}
