package ru.kpfu.itis.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.forms.InstrumentCreateForm;
import ru.kpfu.itis.app.forms.InstrumentEditForm;
import ru.kpfu.itis.app.model.Instrument;
import ru.kpfu.itis.app.repositories.InstrumentRepository;
import ru.kpfu.itis.app.repositories.JobAppRepository;
import ru.kpfu.itis.app.repositories.ParticipantRepository;
import ru.kpfu.itis.app.services.InstrumentService;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Maxim Yagafarov
 **/
@Service
public class InstrumentServiceImpl implements InstrumentService {

    @Autowired
    private InstrumentRepository instrumentRepository;

    @Autowired
    private JobAppRepository jobAppRepository;

    @Autowired
    private ParticipantRepository participantRepository;

    @Override
    public Instrument updateInfo(InstrumentEditForm form) {
        Optional<Instrument> instrumentOptional = instrumentRepository.findOneById(form.getId());
        if (!instrumentOptional.isPresent()) return null;
        Instrument instrument = instrumentOptional.get();
        instrument.setName(form.getName());
        instrument.setDescription(form.getDescription());
        return instrumentRepository.save(instrument);
    }

    @Override
    public void deleteInstrument(Long id) {
        Optional<Instrument> opt = instrumentRepository.findOneById(id);
        if (!opt.isPresent()) return;
        Instrument inst = opt.get();
        jobAppRepository.delete(
                jobAppRepository.findAllByInstrument(inst)
        );
        participantRepository.save(
                participantRepository.findAll().
                        stream().
                        map((q) -> {
                            q.getInstruments().remove(inst);
                            return q;
                        }).collect(Collectors.toList())
        );
        instrumentRepository.delete(id);
    }

    @Override
    public Instrument create(InstrumentCreateForm form) {
        Instrument instrument = Instrument.builder()
                .name(form.getName())
                .description(form.getDescription())
                .build();
        return instrumentRepository.save(instrument);
    }
}
