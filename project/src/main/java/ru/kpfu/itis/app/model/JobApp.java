package ru.kpfu.itis.app.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Maxim Yagafarov
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Entity
@ToString
@Table(name = "\"jobapp\"")
public class JobApp {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "author_id")
    private ParticipantData author;

    @Column
    private Date date;

    @Column
    private String description;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    private Instrument instrument;

    @Column
    private Integer wantedSalary;

    @Enumerated(EnumType.STRING)
    private JobAppStatus status;

    @Column
    private Date interviewDate;

    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "document_id")
    private Document document;

    @Column
    private String email;
}
