package ru.kpfu.itis.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.model.Instrument;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.ParticipantRepository;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.services.AuthenticationService;
import ru.kpfu.itis.app.services.ParticipantService;

import java.util.Optional;

/**
 * Maxim Yagafarov
 **/

@Service
public class ParticipantServiceImpl implements ParticipantService {

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public void register(User user) {
        ParticipantData participantData =
                ParticipantData.builder().user(user)
                .active(false)
                .build();
        participantRepository.save(participantData);
    }

    @Override
    public Optional<ParticipantData> getDataByUser(User user) {
        return participantRepository.findOneByUser(user);
    }

    @Override
    public void addInstrument(User user, Instrument instrument) {
        ParticipantData participant = participantRepository.findOneByUser(user).get();
        participant.getInstruments().add(instrument);
        participantRepository.save(participant);
    }

    @Override
    public Optional<ParticipantData> findOneByLogin(String login) {
        Optional<User> user = usersRepository.findOneByLogin(login);
        if (!user.isPresent()) return Optional.empty();
        return participantRepository.findOneByUser(user.get());
    }

    @Override
    public Optional<ParticipantData> getUserByAuthentication(Authentication authentication) {
        User user = authenticationService.getUserByAuthentication(authentication);
        return participantRepository.findOneByUser(user);
    }
}
