package ru.kpfu.itis.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.app.model.Document;

import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface DocumentRepository extends JpaRepository<Document, Long> {
    Optional<Document> findOneById(Long id);

}
