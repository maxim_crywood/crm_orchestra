package ru.kpfu.itis.app.services.impl;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.app.forms.DocumentCreateForm;
import ru.kpfu.itis.app.forms.DocumentEditForm;
import ru.kpfu.itis.app.model.Document;
import ru.kpfu.itis.app.repositories.DocumentRepository;
import ru.kpfu.itis.app.repositories.JobAppRepository;
import ru.kpfu.itis.app.services.DocumentService;

import javax.print.Doc;
import java.io.DataOutput;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Maxim Yagafarov
 **/

@Service
public class DocumentServiceImpl implements DocumentService{

    @Autowired
    private DocumentRepository documentRepository;

    @Override
    @SneakyThrows
    @Transactional
    public Document saveDocument(MultipartFile file) {
        Document document = Document.builder()
                .content(file.getBytes())
                .build();
        return documentRepository.save(document);
    }

    @Override
    @Transactional
    public Optional<Document> findById(Long id) {
        return documentRepository.findOneById(id);
    }

    @Override
    @Transactional
    public List<Document> findAll() {
        return documentRepository.findAll();
    }

    @Override
    @Transactional
    public Optional<Document> findOneById(Long id) {
        return documentRepository.findOneById(id);
    }

    @Autowired
    private JobAppRepository jobAppRepository;

    @Override
    @Transactional
    public void deleteInstrument(Long id) {
        Optional<Document> opt = documentRepository.findOneById(id);
        if (!opt.isPresent()) return;
        Document doc = opt.get();
        jobAppRepository.delete(
                jobAppRepository.findAllByDocument(doc)
        );
        if (documentRepository.findOneById(id).isPresent())
            documentRepository.delete(id);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void updateInfo(DocumentEditForm form) {
        Optional<Document> opt = documentRepository.findOneById(form.getId());
        if (!opt.isPresent()) return;
        Document doc = opt.get();
        doc.setContent(form.getFile().getBytes());
        documentRepository.save(doc);
    }

    @Override
    @Transactional
    public Document create(DocumentCreateForm form) {
        return saveDocument(form.getFile());
    }
}
