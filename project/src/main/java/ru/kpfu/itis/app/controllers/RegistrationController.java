package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.app.forms.UserRegistrationForm;
import ru.kpfu.itis.app.services.RegistrationService;
import ru.kpfu.itis.app.validators.UserRegistrationFormValidator;

import javax.validation.Valid;

/**
 * Maxim Yagafarov
 **/

@Controller
public class RegistrationController {

    @Autowired
    private RegistrationService service;


    @Autowired
    private UserRegistrationFormValidator userRegistrationFormValidator;

    @InitBinder("userForm")
    public void initUserFormValidator(WebDataBinder binder) {
        binder.addValidators(userRegistrationFormValidator);
    }


    @PostMapping(value = "/register")
    public String register(@Valid @ModelAttribute("userForm") UserRegistrationForm userRegistrationForm,
                           BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/register";
        }

        service.register(userRegistrationForm);
        return "redirect:/main";
    }

    @GetMapping(value = "/register")
    public String getRegisterPage() {
        //   return "redirect:/main";
        return "register";
    }

}
