package ru.kpfu.itis.app.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.app.forms.AddConversationForm;
import ru.kpfu.itis.app.forms.AddMessageForm;
import ru.kpfu.itis.app.forms.ManagerEditForm;
import ru.kpfu.itis.app.forms.UserRegistrationForm;
import ru.kpfu.itis.app.model.Conversation;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.ConversationRepository;
import ru.kpfu.itis.app.repositories.ParticipantRepository;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.services.AuthenticationService;

import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
@Component
public class AddMessageValidator implements Validator {

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(AddMessageForm.class.getName());
    }

    @Transactional
    @Override
    public void validate(Object target, Errors errors) {
        AddMessageForm form = (AddMessageForm) target;
        Optional<Conversation> conv = conversationRepository.findOneById(form.getConversationId());
        if (!conv.isPresent()) {
            errors.reject("no such conversation");
        }
    }

}
