package ru.kpfu.itis.app.model;

import lombok.*;
import ru.kpfu.itis.app.security.role.Role;

import javax.persistence.*;
import java.util.List;

/**
 * Maxim Yagafarov
 **/

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Entity
@ToString
@Table(name = "\"instrument\"")
public class Instrument {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String name;

    @Column
    private String description;

}
