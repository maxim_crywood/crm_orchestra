package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.AccessType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.app.forms.QuestionCreateForm;
import ru.kpfu.itis.app.forms.QuestionEditForm;
import ru.kpfu.itis.app.forms.UserCreateForm;
import ru.kpfu.itis.app.forms.UserEditForm;
import ru.kpfu.itis.app.model.Question;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.QuestionRepository;
import ru.kpfu.itis.app.security.role.Role;
import ru.kpfu.itis.app.services.QuestionService;
import ru.kpfu.itis.app.validators.QuestionCreateFormValidator;
import ru.kpfu.itis.app.validators.QuestionEditFormValidator;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Maxim Yagafarov
 **/

@Controller
public class AdminQuestionCRUDController {


    @Autowired
    private QuestionCreateFormValidator questionCreateFormValidator;

    @InitBinder("questionCreateForm")
    public void initUserRegistrationFormValidator(WebDataBinder binder) {
        binder.addValidators(questionCreateFormValidator);
    }

    @Autowired
    private QuestionEditFormValidator questionEditFormValidator;

    @InitBinder("questionEditForm")
    public void initUser2RegistrationFormValidator(WebDataBinder binder) {
        binder.addValidators(questionEditFormValidator);
    }

    private QuestionService questionService;

    public AdminQuestionCRUDController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("admin/questions")
    public String getQuestions(@ModelAttribute("model")ModelMap model) {
        List<Question> questions = questionService.findAll();
        Collections.sort(questions, Comparator.comparing(Question::getDate));
        Collections.reverse(questions);
        model.addAttribute("questions", questions);
        return "admin_questions";
    }

    @GetMapping("/admin/question")
    public String getQuestion(@ModelAttribute("model") ModelMap model,
                          @RequestParam Long id) {
        Optional<Question> questionOpt = questionService.findOneById(id);
        if (!questionOpt.isPresent()) return "redirect:/questions";
        Question question = questionOpt.get();
        model.addAttribute("question", question);
        return "admin_question";
    }

    @PostMapping("/admin/question/delete")
    public String deleteQuestion(@RequestParam Long id) {
        questionService.deleteQuestion(id);
        return "redirect:/admin/questions";
    }

    @PostMapping("admin/question/edit")
    public String editUser(@Valid @ModelAttribute("questionEditForm")QuestionEditForm questionEditForm,
                           BindingResult result,
                           RedirectAttributes attributes) {
        if (result.hasErrors()) return "redirect:/admin/questions";
        questionService.updateInfo(questionEditForm);
        attributes.addAttribute("id", questionEditForm.getId());
        return "redirect:/admin/question";
    }

    @GetMapping("/admin/question/creation")
    public String getCreationPage(@ModelAttribute("model") ModelMap model) {
        return "admin_question_creation";
    }

    @PostMapping("/admin/question/create")
    public String create(@Valid @ModelAttribute("questionCreateForm") QuestionCreateForm questionCreateForm,
                         BindingResult result,
                         RedirectAttributes attributes) {
        if (result.hasErrors()) return "redirect:/admin/question/creation";
        Question question = questionService.create(questionCreateForm);
        attributes.addAttribute("id", question.getId());
        return "redirect:/admin/question";
    }
}
