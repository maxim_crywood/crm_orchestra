package ru.kpfu.itis.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.app.model.*;
import ru.kpfu.itis.app.services.JobAppService;

import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface JobAppRepository extends JpaRepository<JobApp, Long> {

    Optional<JobApp> findOneById(Long id);
    List<JobApp> findAllByAuthor(ParticipantData author);

    List<JobApp> findAllByStatus(JobAppStatus status);

    List<JobApp> findAllByInstrument(Instrument instrument);

    List<JobApp> findAllByDocument(Document doc);
}
