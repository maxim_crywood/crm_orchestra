package ru.kpfu.itis.app.services;

import ru.kpfu.itis.app.forms.ManagerEditForm;
import ru.kpfu.itis.app.forms.UserCreateForm;
import ru.kpfu.itis.app.forms.UserEditForm;
import ru.kpfu.itis.app.model.User;

import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface UserService {
    Optional<User> findOneByLogin(String login);

    void updateInfo(ManagerEditForm managerEditForm, User user);

    void deleteUser(Long id);

    Optional<User> updateInfo(UserEditForm userEditForm);

    User create(UserCreateForm userEditForm);
}
