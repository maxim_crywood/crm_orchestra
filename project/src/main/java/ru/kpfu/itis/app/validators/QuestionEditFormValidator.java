package ru.kpfu.itis.app.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.app.forms.QuestionCreateForm;
import ru.kpfu.itis.app.forms.QuestionEditForm;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.UsersRepository;

import java.util.Optional;

@Component
public class QuestionEditFormValidator implements Validator {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(QuestionEditForm.class.getName());
    }

    @Transactional
    @Override
    public void validate(Object target, Errors errors) {
        QuestionEditForm form = (QuestionEditForm) target;

        Optional<User> existedUser = usersRepository.findOneByLogin(form.getLogin());

        if (!existedUser.isPresent()) {
            errors.reject("bad.login", "Логинa net");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "empty.login", "Пустой логин");
    }

}