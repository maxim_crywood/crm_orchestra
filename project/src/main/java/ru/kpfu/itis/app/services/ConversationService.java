package ru.kpfu.itis.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import ru.kpfu.itis.app.forms.AddMessageForm;
import ru.kpfu.itis.app.model.Conversation;
import ru.kpfu.itis.app.model.Message;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.repositories.ConversationRepository;

import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface ConversationService {

    List<Conversation> findAllByParticipant(ParticipantData user);

    Optional<Conversation> findOneById(Long id);

    List<Message> getMessages(Conversation conversation);

    Conversation createConversation(ParticipantData user1, ParticipantData user2);

    Message createMessage(AddMessageForm form, ParticipantData author);
}
