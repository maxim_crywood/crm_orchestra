package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * Maxim Yagafarov
 **/


@Controller
public class MainPageController {

    @Autowired
    private UserHelper userHelper;

    @GetMapping("/manager/main")
    public String getManagerMain() {
        return "manager_main";
    }

    @GetMapping("/participant/main")
    public String getParticipantPage(@ModelAttribute("model") ModelMap model, Authentication auth) {
        userHelper.prepareParticipantView(model, auth);
        return "participant_base";
    }
    @GetMapping("/participant/test")
    public String getTest() {
        return "index";
    }

}
