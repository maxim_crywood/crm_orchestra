package ru.kpfu.itis.app.services;

import org.springframework.security.core.Authentication;
import ru.kpfu.itis.app.model.Instrument;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.User;

import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface ParticipantService {
    void register(User user);

    Optional<ParticipantData> getDataByUser(User userByAuthentication);

    void addInstrument(User user, Instrument instrument);

    Optional<ParticipantData> findOneByLogin(String login);

    Optional<ParticipantData> getUserByAuthentication(Authentication authentication);
}
