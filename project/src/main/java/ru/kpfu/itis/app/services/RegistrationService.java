package ru.kpfu.itis.app.services;


import ru.kpfu.itis.app.forms.UserRegistrationForm;

/**
 * Maxim Yagafarov
 **/
public interface RegistrationService {
    void register(UserRegistrationForm userForm);
}
