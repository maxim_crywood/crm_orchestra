package ru.kpfu.itis.app.controllers;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.app.forms.DocumentCreateForm;
import ru.kpfu.itis.app.forms.DocumentEditForm;
import ru.kpfu.itis.app.forms.InstrumentCreateForm;
import ru.kpfu.itis.app.forms.InstrumentEditForm;
import ru.kpfu.itis.app.model.*;
import ru.kpfu.itis.app.repositories.InstrumentRepository;
import ru.kpfu.itis.app.security.role.Role;
import ru.kpfu.itis.app.services.DocumentService;
import ru.kpfu.itis.app.services.InstrumentService;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/

@Controller
public class AdminDocumentCRUDController {

    @Autowired
    private DocumentService documentService;

    @GetMapping("/admin/documents")
    public String getInstruments(@ModelAttribute("model")ModelMap model) {
        model.addAttribute("documents", documentService.findAll());
        return "admin_documents";
    }

    @GetMapping("/admin/document")
    public String getUser(@ModelAttribute("model") ModelMap model,
                          @RequestParam Long id) {

        model.addAttribute("document", documentService.findOneById(id).get());
        return "admin_document";
    }

    @PostMapping("/admin/document/delete")
    public String deleteUser(@RequestParam Long id) {
        documentService.deleteInstrument(id);
        return "redirect:/admin/documents";
    }

    @PostMapping("admin/document/edit")
    public String editUser(@ModelAttribute("documentEditForm")DocumentEditForm documentEditForm,
                           RedirectAttributes attributes) {
        documentService.updateInfo(documentEditForm);
        attributes.addAttribute("id", documentEditForm.getId());
        return "redirect:/admin/document";
    }

    @GetMapping("/admin/document/creation")
    public String getCreationPage(@ModelAttribute("model") ModelMap model) {
        return "admin_document_creation";
    }

    @PostMapping("/admin/document/create")
    public String create(@ModelAttribute("documentCreateForm") DocumentCreateForm documentCreateForm,
                         RedirectAttributes attributes) {
        Document document = documentService.create(documentCreateForm);
        attributes.addAttribute("id", document.getId());
        return "redirect:/admin/document";
    }

    @GetMapping("admin/download_cv")
    @SneakyThrows
    public String getCv(
            @RequestParam Long id,
            HttpServletResponse response) {
            Optional<Document> documentOpt = documentService.findById(id);
            if (!documentOpt.isPresent())
                return "redirect:/";
            Document document = documentOpt.get();response.setContentType("application/pdf");
            response.setContentLength(document.getContent().length);
            FileCopyUtils.copy(document.getContent(), response.getOutputStream());
            return "redirect:/";
        }
}
