package ru.kpfu.itis.app.services;

import ru.kpfu.itis.app.forms.JobAppForm;
import ru.kpfu.itis.app.model.JobApp;
import ru.kpfu.itis.app.model.JobAppStatus;
import ru.kpfu.itis.app.model.ParticipantData;

import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface JobAppService {
    JobApp createJobApp(JobAppForm form, ParticipantData author);

    Optional<JobApp> findOneBy(Long id);

    List<JobApp> findAllByAuthor(ParticipantData author);

    List<JobApp> findAllByStatus(JobAppStatus status);

    List<JobApp> findAll();

    void changeStatus(JobApp jobApp, JobAppStatus status);
}
