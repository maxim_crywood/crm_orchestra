package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.app.forms.JobAppStatusFilterForm;
import ru.kpfu.itis.app.model.JobApp;
import ru.kpfu.itis.app.model.JobAppStatus;
import ru.kpfu.itis.app.services.JobAppService;

import java.util.*;

/**
 * Maxim Yagafarov
 **/
@Controller
public class ManagarJobAppController {
    @Autowired
    private JobAppService jobAppService;

    @Autowired
    private AuthController authController;

    @Autowired
    private UserHelper userHelper;

    @GetMapping("/manager/concrete_jobapp")
    public String getJobapp(@ModelAttribute("model") ModelMap model,
                            @RequestParam Long id,
                            Authentication authentication) {
        userHelper.prepareParticipantView(model, authentication);
        Optional<JobApp> jobAppOpt = jobAppService.findOneBy(id);
        if (!jobAppOpt.isPresent())
            return "redirect:/";
        JobApp jobApp = jobAppOpt.get();
        model.addAttribute("jobapp", jobApp);
        addStatuses(model);
        model.addAttribute("sobes", jobApp.getStatus().equals(JobAppStatus.INTERVIEW));
        return "manager_concrete_jobapp";
    }

    @PostMapping("/manager/set_interview")
    public String setInterview(@RequestParam String date) {
        System.out.println("date = " + date);
        return "manager_profile";
    }

    @PostMapping("/manager/change_jobapp_status")
    public String changeJobAppStatus(@RequestParam String status,
                                     @RequestParam Long id,
                                     RedirectAttributes attributes) {
        jobAppService.changeStatus(jobAppService.findOneBy(id).get(), JobAppStatus.getByText(status));
        attributes.addAttribute("id", id);

        return "redirect:/manager/concrete_jobapp";
    }


    @GetMapping("/manager/all_jobapps")
    public String showJobAppsByStatus(@ModelAttribute("model") ModelMap model,
                                       @RequestParam Optional<String> status,
                                      Authentication authentication) {

        userHelper.prepareParticipantView(model, authentication);
        System.out.println("in showJbAppsByStatus");
        List<JobApp> jobApps;
        if (status.isPresent()) {
            System.out.println("present");
            System.out.println("jobAppStatusFilterForm = " + status.get());
            jobApps = jobAppService.findAllByStatus(
                    JobAppStatus.getByText(status.get()));
        } else {
            System.out.println("not present");
            jobApps = jobAppService.findAll();
        }
        model.put("jobapps", jobApps);
        addStatuses(model);
        return "manager_all_jobapps";
    }

    private void addStatuses(ModelMap model) {
        JobAppStatus[] statuses = JobAppStatus.values();
        List<String> strings = new ArrayList<>();
        for (JobAppStatus st : statuses)
            strings.add(st.getText());
        model.put("statuses", strings);
    }
}
