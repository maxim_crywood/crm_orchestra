package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.ui.ModelMap;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.services.AuthenticationService;
import ru.kpfu.itis.app.services.ParticipantService;
import ru.kpfu.itis.app.services.UserService;

/**
 * Maxim Yagafarov
 **/
public class UserHelper {
    @Autowired
    private AuthenticationService authentictionService;

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private UserService userService;

    public void prepareParticipantView(ModelMap model, Authentication authentication) {
        User user = authentictionService.getUserByAuthentication(authentication);
//        System.out.println("in participantHelper");
//        System.out.println("user = " + user);

        model.addAttribute("user", user);
    }
}
