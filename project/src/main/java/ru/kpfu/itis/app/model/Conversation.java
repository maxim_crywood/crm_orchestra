package ru.kpfu.itis.app.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Maxim Yagafarov
 **/

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Entity
@ToString
@Table(name = "\"conversation\"")
public class Conversation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Date date;

    @ManyToOne()
    @JoinColumn(name="first_id")
    private ParticipantData user1;

    @ManyToOne
    @JoinColumn(name="second_id")
    private ParticipantData user2;

    @OneToMany(cascade = CascadeType.REMOVE)
    private List<Message> messages;
}
