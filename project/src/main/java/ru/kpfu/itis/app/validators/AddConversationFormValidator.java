package ru.kpfu.itis.app.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.app.forms.AddConversationForm;
import ru.kpfu.itis.app.forms.ManagerEditForm;
import ru.kpfu.itis.app.forms.UserRegistrationForm;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.ParticipantRepository;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.services.AuthenticationService;

import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
@Component
public class AddConversationFormValidator implements Validator {

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(AddConversationForm.class.getName());
    }

    @Transactional
    @Override
    public void validate(Object target, Errors errors) {
        AddConversationForm form = (AddConversationForm) target;

        Optional<User> user = usersRepository.findOneByLogin(form.getLogin());
        if (!user.isPresent()) {
            errors.reject("bad.login", "Логин не существует");
            return;
        }
        Optional<ParticipantData> existedUser = participantRepository.findOneByUser(user.get());

        if (!existedUser.isPresent()) {
            errors.reject("bad.login", "Логин не существует");
        }
    }

}
