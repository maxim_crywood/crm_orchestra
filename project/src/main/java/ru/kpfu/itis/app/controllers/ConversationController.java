package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.app.forms.AddConversationForm;
import ru.kpfu.itis.app.forms.AddMessageForm;
import ru.kpfu.itis.app.forms.UserRegistrationForm;
import ru.kpfu.itis.app.model.Conversation;
import ru.kpfu.itis.app.model.Message;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.services.AuthenticationService;
import ru.kpfu.itis.app.services.ConversationService;
import ru.kpfu.itis.app.services.ParticipantService;
import ru.kpfu.itis.app.validators.AddConversationFormValidator;
import ru.kpfu.itis.app.validators.AddMessageValidator;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/

@Controller
public class ConversationController {

    @Autowired
    private AddConversationFormValidator addConversationFormValidator;

    @Autowired
    private AddMessageValidator addMessageValidator;

    @Autowired
    private ConversationService conversationService;

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UsersRepository usersRepository;

    @InitBinder("addMessageForm")
    public void initAddMessageForm(WebDataBinder binder) {
        binder.addValidators(addMessageValidator);
    }

    @InitBinder("addConversationForm")
    public void initaddConversationFormValidator(WebDataBinder binder) {
        binder.addValidators(addConversationFormValidator);
    }


    @GetMapping("/participant/all_conversations")
    public String getConversations(
            @ModelAttribute("model") ModelMap model,
            Authentication authentication,
            @RequestParam Optional<String> error) {
        ParticipantData user = participantService.getDataByUser(
                authenticationService.getUserByAuthentication(authentication)
        ).get();
        model.addAttribute("conversations",
                conversationService.findAllByParticipant(user));
        model.addAttribute("user", user);
        model.addAttribute("error", error);
        return "conversations";
    }

    @GetMapping(value = "/participant/add_conversation")
    public String addConversation(
            @Valid @ModelAttribute("addConversationForm") AddConversationForm addConversationForm,
            BindingResult errors,
            RedirectAttributes attributes,
            Authentication authentication) {
        if (errors.hasErrors()) {
            attributes.addAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/participant/all_conversations";
        }
        ParticipantData participant = participantService.findOneByLogin(addConversationForm.getLogin()).get();
        Conversation conversation = conversationService.createConversation(participant,
                participantService.getUserByAuthentication(authentication).get());
        attributes.addAttribute("id",
                conversation.getId());
        return "redirect:/participant/conversation";
    }

    @GetMapping("/participant/conversation")
    public String showConversation(
            @ModelAttribute("model") ModelMap model,
            @RequestParam Long id,
            Authentication authentication
    ) {
        Optional<Conversation> conversationOptional = conversationService.findOneById(id);
        if (!conversationOptional.isPresent()) {
            model.addAttribute("error", Optional.of("Такоц беседы не существует"));
        } else {
            model.addAttribute("error", Optional.empty());
        }
        ParticipantData user = participantService.getUserByAuthentication(authentication).get();

        Conversation conversation = conversationOptional.get();
        if (!user.getId().equals(conversation.getUser1().getId()) &&
                !user.getId().equals(conversation.getUser2().getId())) {
            return "redirect:/";
        }
        Collections.sort(conversation.getMessages(), Comparator.comparing(Message::getDate));
        model.addAttribute("conversation", conversation);
        ParticipantData user1 = participantService.getUserByAuthentication(authentication).get();
        model.addAttribute("user1", user1);
        model.addAttribute("user2",
                conversation.getUser1().getId().equals(user1.getId()) ?
                        conversation.getUser2() :
                        conversation.getUser1()
        );
        List<Message> kek = null;
        kek.stream().filter(a -> a.getText().equals("kek")).findAny();
        return "conversation";
    }

    @PostMapping("/participant/add_message")
    public String addMessage(
            @ModelAttribute("addMessageForm") @Valid AddMessageForm form,
            Authentication authentication,
            BindingResult result,
            RedirectAttributes attributes) {
        if (result.hasErrors()) {
            return "redirect:/participant/all_conversations";
        }
        conversationService.createMessage(form, participantService.getUserByAuthentication(authentication).get());
        attributes.addAttribute("id", form.getConversationId());
        return "redirect:/participant/conversation";
    }
}
