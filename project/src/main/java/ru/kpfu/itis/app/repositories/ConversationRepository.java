package ru.kpfu.itis.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.app.model.Conversation;
import ru.kpfu.itis.app.model.ParticipantData;

import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface ConversationRepository extends JpaRepository<Conversation, Long> {
    Optional<Conversation> findOneById(Long id);
    List<Conversation> findAllByUser1(ParticipantData user);
    List<Conversation> findAllByUser2(ParticipantData user);
    Optional<Conversation> findFirstByUser1(ParticipantData user);
    Optional<Conversation> findFirstByUser2(ParticipantData user);
}
