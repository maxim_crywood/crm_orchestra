package ru.kpfu.itis.app.validators;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.app.forms.AddConversationForm;
import ru.kpfu.itis.app.forms.JobAppForm;
import ru.kpfu.itis.app.forms.ManagerEditForm;
import ru.kpfu.itis.app.forms.UserRegistrationForm;
import ru.kpfu.itis.app.model.JobApp;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.ParticipantRepository;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.services.AuthenticationService;

import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
@Component
public class JobAppFormValidator implements Validator {

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(JobAppForm.class.getName());
    }

    @Transactional
    @Override
    @SneakyThrows
    public void validate(Object target, Errors errors) {
        JobAppForm form = (JobAppForm) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "empty.email", "Пустой email");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description","empty.description", "lol");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "file","empty.file","lo");
        if (form.getFile() == null ||
                form.getFile().getBytes().length == 0
                ) {
            errors.reject("missing file");
            return;
        }
        if (!form.getFile().getContentType().equals("application/pdf")) {
            errors.reject("file must be pdf");
            return;
        }
    }

}
