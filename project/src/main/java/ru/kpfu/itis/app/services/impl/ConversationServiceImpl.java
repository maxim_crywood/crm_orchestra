package ru.kpfu.itis.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.forms.AddMessageForm;
import ru.kpfu.itis.app.model.Conversation;
import ru.kpfu.itis.app.model.Message;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.repositories.ConversationRepository;
import ru.kpfu.itis.app.repositories.MessageRepository;
import ru.kpfu.itis.app.services.ConversationService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Maxim Yagafarov
 **/
@Service
public class ConversationServiceImpl implements ConversationService {

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Override
    public List<Conversation> findAllByParticipant(ParticipantData user) {
        List<Conversation> ret = conversationRepository.findAllByUser1(user);
        ret.addAll(conversationRepository.findAllByUser2(user));
        Collections.sort(ret, Comparator.comparing(Conversation::getDate));
        Collections.reverse(ret);
        return ret.stream().distinct().collect(Collectors.toList());
    }

    @Override
    public Optional<Conversation> findOneById(Long id) {
        return conversationRepository.findOneById(id);
    }

    @Override
    public List<Message> getMessages(Conversation conversation) {
        List<Message> ret = messageRepository.findAllByConversation(conversation);
        Collections.sort(ret, Comparator.comparing(Message::getDate));
        Collections.reverse(ret);
        return ret;
    }

    @Override
    public Conversation createConversation(ParticipantData user1, ParticipantData user2) {
        Conversation conversation = Conversation.builder()
                .user1(user1)
                .user2(user2)
                .date(new Date(System.currentTimeMillis()))
                .build();
        conversation = conversationRepository.save(conversation);
        return conversation;
    }

    @Override
    public Message createMessage(AddMessageForm form, ParticipantData author) {
        Conversation conversation = conversationRepository.findOneById(form.getConversationId()).get();
        Message msg = Message.builder()
                .author(author)
                .conversation(conversation)
                .date(new Date(System.currentTimeMillis()))
                .text(form.getText())
                .build();
        msg = messageRepository.save(msg);
        conversation.getMessages().add(msg);
        conversationRepository.save(conversation);
        return msg;
    }
}
