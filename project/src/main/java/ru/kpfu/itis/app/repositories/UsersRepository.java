package ru.kpfu.itis.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.app.model.User;

import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/

public interface UsersRepository extends JpaRepository<User,Long> {
    List<User> findAll();
    Optional<User> findOneByLogin(String username);

    Optional<User> findOneByEmail(String email);

    Optional<User> findOneById(Long id);
}
