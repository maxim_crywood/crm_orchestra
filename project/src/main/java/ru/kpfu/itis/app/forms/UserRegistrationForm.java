package ru.kpfu.itis.app.forms;

import lombok.*;

/**
 * Maxim Yagafarov
 **/

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserRegistrationForm {
    private String login;
    private String password;
    private String email;
    private String role;
}