package ru.kpfu.itis.app.services;

import ru.kpfu.itis.app.forms.InstrumentCreateForm;
import ru.kpfu.itis.app.forms.InstrumentEditForm;
import ru.kpfu.itis.app.model.Instrument;

/**
 * Maxim Yagafarov
 **/
public interface InstrumentService {

    Instrument updateInfo(InstrumentEditForm form);

    void deleteInstrument(Long id);

    Instrument create(InstrumentCreateForm form);
}
