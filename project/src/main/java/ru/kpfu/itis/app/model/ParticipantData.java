package ru.kpfu.itis.app.model;

import lombok.*;
import ru.kpfu.itis.app.security.role.Role;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Maxim Yagafarov
 **/

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Entity
//@ToString
@Table(name = "\"participant_data\"")
public class ParticipantData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name = "participant_instrument",
            joinColumns = @JoinColumn(name = "participant_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "instrument_id", referencedColumnName = "id")
    )
    private Set<Instrument> instruments;

    @Column
    @NotNull
    private Boolean active;

    @Override
    public String toString() {
        return "ParticipantData{id=" + id + ", user = " + user + "}";
    }
}
