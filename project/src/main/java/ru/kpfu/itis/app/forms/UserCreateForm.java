package ru.kpfu.itis.app.forms;

import lombok.*;

/**
 * Maxim Yagafarov
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserCreateForm {
    private String login;
    private String email;
    private String role;
    private String password;
}
