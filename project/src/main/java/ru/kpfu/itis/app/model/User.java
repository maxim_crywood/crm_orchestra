package ru.kpfu.itis.app.model;

import lombok.*;
import ru.kpfu.itis.app.security.role.Role;

import javax.persistence.*;

/**
 * Maxim Yagafarov
 **/

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Entity
@ToString
@Table(name = "\"user\"")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String login;

    @Column(unique = true)
    private String email;

    private String hashPassword;

    @Enumerated(EnumType.STRING)
    private Role role;

}
