package ru.kpfu.itis.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.forms.UserRegistrationForm;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.security.role.Role;
import ru.kpfu.itis.app.services.ParticipantService;
import ru.kpfu.itis.app.services.RegistrationService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * Maxim Yagafarov
 **/
@Service
public class RegistrationServiceImpl implements RegistrationService {


    @Autowired
    private ParticipantService participantService;

    @Autowired
    private UsersRepository usersRepository;


    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public void register(UserRegistrationForm userForm) {
        User newUser = User.builder()
                .login(userForm.getLogin())
                .email(userForm.getEmail())
                .hashPassword(passwordEncoder.encode(userForm.getPassword()))
                .role(Role.fromString(userForm.getRole()))
                .build();
        usersRepository.save(newUser);
        if (newUser.getRole() == Role.POTENTIAL_PARTICIPANT) {
            newUser = usersRepository.findOneByLogin(newUser.getLogin()).get();
            participantService.register(newUser);
        }
    }
}
