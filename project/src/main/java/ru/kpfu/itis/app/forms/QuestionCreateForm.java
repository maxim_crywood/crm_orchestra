package ru.kpfu.itis.app.forms;

import lombok.*;

/**
 * Maxim Yagafarov
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class QuestionCreateForm {
    private String login;
    private String content;
}
