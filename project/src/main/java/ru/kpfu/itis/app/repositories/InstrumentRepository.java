package ru.kpfu.itis.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.app.model.Instrument;

import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface InstrumentRepository extends JpaRepository<Instrument, Long> {
    List<Instrument> findAll();
    Optional<Instrument> findOneById(Long id);

    Optional<Instrument> findOneByName(String name);
}
