package ru.kpfu.itis.app.forms;

import lombok.*;

/**
 * Maxim Yagafarov
 **/

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AddConversationForm {
    private String login;
}
