package ru.kpfu.itis.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import ru.kpfu.itis.app.controllers.UserHelper;

@EnableWebSocket
@SpringBootApplication
@EnableJpaRepositories(basePackages = "ru.kpfu.itis.app.repositories")
@EntityScan(basePackages = "ru.kpfu.itis.app.model")
public class Application extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        // конфигурирует статику
        return application.sources(Application.class);
    }

    @Bean(name = "participantHelper")
    public UserHelper participantHelper() {
        return new UserHelper();
    }

    @Bean(name = "multipartResolver")
    public StandardServletMultipartResolver resolver(){
        return new StandardServletMultipartResolver();
    }


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
