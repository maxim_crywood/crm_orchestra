package ru.kpfu.itis.app.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Maxim Yagafarov
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Entity
@ToString
@Table(name = "\"message\"")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private Date date;

    @Column
    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="conversation_id")
    private Conversation conversation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="author_id")
    private ParticipantData author;
}
