package ru.kpfu.itis.app.forms;

import lombok.*;

/**
 * Maxim Yagafarov
 **/

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class JobAppStatusFilterForm {
    private String status;
}
