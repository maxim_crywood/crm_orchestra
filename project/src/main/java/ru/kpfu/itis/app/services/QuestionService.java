package ru.kpfu.itis.app.services;

import ru.kpfu.itis.app.forms.QuestionCreateForm;
import ru.kpfu.itis.app.forms.QuestionEditForm;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.Question;
import ru.kpfu.itis.app.model.User;

import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface QuestionService {
    Optional<Question> findOneById(Long id);
    List<Question> findAllByAuthor(ParticipantData author);
    List<Question> findAll();

    Question addQuestion(String content, ParticipantData user);

    Question answer(Question question, String answer, User user);

    Question create(QuestionCreateForm form);

    Question updateInfo(QuestionEditForm form);

    void deleteQuestion(Long id);
}
