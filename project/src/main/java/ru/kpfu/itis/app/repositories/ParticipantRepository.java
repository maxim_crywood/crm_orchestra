package ru.kpfu.itis.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.User;

import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface ParticipantRepository extends JpaRepository<ParticipantData, Long> {

    Optional<ParticipantData> findOneByUser(User user);
}
