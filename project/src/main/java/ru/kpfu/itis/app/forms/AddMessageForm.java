package ru.kpfu.itis.app.forms;

import lombok.*;

/**
 * Maxim Yagafarov
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AddMessageForm {
    private String text;
    private Long conversationId;
}
