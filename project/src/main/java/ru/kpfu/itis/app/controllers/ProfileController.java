package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.app.forms.ManagerEditForm;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.InstrumentRepository;
import ru.kpfu.itis.app.services.AuthenticationService;
import ru.kpfu.itis.app.services.ManagerService;
import ru.kpfu.itis.app.services.ParticipantService;
import ru.kpfu.itis.app.services.UserService;
import ru.kpfu.itis.app.validators.ManagerEditFormValidator;

import javax.validation.Valid;

/**
 * Maxim Yagafarov
 **/

@Controller
public class ProfileController {

    @Autowired
    private UserHelper helper;

    @Autowired
    private UserService userService;
    @Autowired
    private InstrumentRepository instrumentRepository;

    @Autowired
    private ManagerService managerService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ManagerEditFormValidator managerEditFormValidator;

    @Autowired
    private ParticipantService participantService;


    @InitBinder("managerEditForm")
    public void initUserRegistrationFormValidator(WebDataBinder binder) {
        binder.addValidators(managerEditFormValidator);
    }

    @GetMapping("/manager/profile")
    public String getManagerProfile(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        User user = authenticationService.getUserByAuthentication(authentication);
        model.addAttribute("user", user);
        return "manager_profile";
    }

    @PostMapping("/manager/profile")
    public String editManagerProfile(@Valid @ModelAttribute("managerEditForm") ManagerEditForm managerEditForm,
                                     BindingResult errors,
                                     Authentication authentication,
                                     RedirectAttributes attributes) {

        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/manager/profile";
        }
        userService.updateInfo(managerEditForm, authenticationService.getUserByAuthentication(authentication));
        return "redirect:/manager/profile";

    }

    @PostMapping("/participant/profile")
    public String editParticipantProfile(@Valid @ModelAttribute("managerEditForm") ManagerEditForm managerEditForm,
                                     BindingResult errors,
                                     Authentication authentication,
                                     RedirectAttributes attributes) {

        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/participant/profile";
        }
        userService.updateInfo(managerEditForm, authenticationService.getUserByAuthentication(authentication));
        return "redirect:/participant/profile";

    }

    @GetMapping("/participant/profile")
    public String getParticipantProfile(@ModelAttribute("model") ModelMap model,
                                        Authentication authentication) {
        helper.prepareParticipantView(model, authentication);
        ParticipantData pUser = participantService
                .getDataByUser(authenticationService.getUserByAuthentication(authentication)).get();
        model.addAttribute("par", pUser);
        model.addAttribute("instruments", instrumentRepository.findAll());
        return "participant_profile";
    }

    @RequestMapping(value = "/participant/add_instrument")
    public @ResponseBody
    ResponseEntity<String> addInstrument(
            @RequestParam String ids, Authentication authentication) {
        System.out.println("in addInstrument");
        Long id = Long.parseLong(ids);
        System.out.println("id = " + id);

        participantService.addInstrument(
                authenticationService.getUserByAuthentication(authentication),
                instrumentRepository.findOneById(id).get());
        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
