package ru.kpfu.itis.app.services;

import ru.kpfu.itis.app.forms.ManagerEditForm;
import ru.kpfu.itis.app.model.User;

/**
 * Maxim Yagafarov
 **/
public interface ManagerService {
    void updateInfo(ManagerEditForm managerEditForm, User user);
}
