package ru.kpfu.itis.app.security.role;

/**
 * Maxim Yagafarov
 **/

public enum Role {
    ADMIN("ADMIN"), MANAGER("MANAGER"), PARTICIPANT("PARTICIPANT"), POTENTIAL_PARTICIPANT("POTENTIAL_PARTICIPANT");

    private String text;

    Role(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public static Role fromString(String text) {
        for (Role b : Role.values()) {
            if (b.text.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }

    public static Role getByText(String text) {
        for (Role role : Role.values()) {
            if (role.text.equals(text)) return role;
        }
        return null;
    }
}