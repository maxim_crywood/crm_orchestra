package ru.kpfu.itis.app.model;

import lombok.*;
import org.springframework.stereotype.Controller;

import javax.persistence.*;
import java.util.Date;

/**
 * Maxim Yagafarov
 **/

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Entity
@ToString
@Table(name = "\"question\"")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String answer;

    @Column
    private Boolean answered = false;

    public String isAnswered() {
        return answered == null ? "no" : (answered ? "yes" : "no");
    }

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "manager_id")
    private User manager;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "author_id")
    private ParticipantData author;

    @Column
    private String content;

    @Column
    private Date date;
}
