package ru.kpfu.itis.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.forms.ManagerEditForm;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.services.ManagerService;

/**
 * Maxim Yagafarov
 **/

@Service
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public void updateInfo(ManagerEditForm managerEditForm, User user) {
        user.setLogin(managerEditForm.getLogin());
        user.setEmail(managerEditForm.getEmail());
        usersRepository.save(user);
    }

}
