package ru.kpfu.itis.app.services;

import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.app.forms.DocumentCreateForm;
import ru.kpfu.itis.app.forms.DocumentEditForm;
import ru.kpfu.itis.app.model.Document;
import ru.kpfu.itis.app.model.Instrument;

import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface DocumentService {
    Document saveDocument(MultipartFile file);

    Optional<Document> findById(Long id);

    List<Document> findAll();

    Optional<Document> findOneById(Long id);

    void deleteInstrument(Long id);

    void updateInfo(DocumentEditForm form);

    Document create(DocumentCreateForm form);
}
