package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.app.forms.ManagerEditForm;
import ru.kpfu.itis.app.forms.UserCreateForm;
import ru.kpfu.itis.app.forms.UserEditForm;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.security.role.Role;
import ru.kpfu.itis.app.services.ParticipantService;
import ru.kpfu.itis.app.services.UserService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Maxim Yagafarov
 **/

@Controller
public class AdminUserCRUDController {

    @Autowired
    private UserService userService;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/admin/main")
    public String getMain() {
        return "admin_main";
    }

    @GetMapping("admin/users")
    public String getUsers(@ModelAttribute("model")ModelMap model) {
        List<User> users = usersRepository.findAll();
        users = users.stream().
                filter((u) -> !u.getRole().equals(Role.ADMIN)).
                sorted(Comparator.comparing(User::getLogin)).
                collect(Collectors.toList());
        model.addAttribute("users", users);
        return "admin_users";
    }

    @GetMapping("/admin/user")
    public String getUser(@ModelAttribute("model") ModelMap model,
                          @RequestParam Long id) {
        Optional<User> userOpt = usersRepository.findOneById(id);
        if (!userOpt.isPresent()) return "redirect:/users";
        User user = userOpt.get();
        model.addAttribute("user", user);
        return "admin_user";
    }

    @PostMapping("/admin/user/delete")
    public String deleteUser(@RequestParam Long id) {
        userService.deleteUser(id);
        return "redirect:/admin/users";
    }

    @PostMapping("admin/user/edit")
    public String editUser(@ModelAttribute("userEditForm")UserEditForm userEditForm,
                           RedirectAttributes attributes) {
        userService.updateInfo(userEditForm);
        attributes.addAttribute("id", userEditForm.getId());
        return "redirect:/admin/user";
    }

    @GetMapping("/admin/user/creation")
    public String getCreationPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("roles", Arrays.asList(Role.values()));
        return "admin_user_creation";
    }

    @PostMapping("/admin/user/create")
    public String create(@ModelAttribute("userCreateForm") UserCreateForm userCreateForm,
                         RedirectAttributes attributes) {
        User user = userService.create(userCreateForm);
        attributes.addAttribute("id", user.getId());
        return "redirect:/admin/user";
    }
}
