package ru.kpfu.itis.app.services.impl;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.app.forms.ManagerEditForm;
import ru.kpfu.itis.app.forms.UserCreateForm;
import ru.kpfu.itis.app.forms.UserEditForm;
import ru.kpfu.itis.app.model.Conversation;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.*;
import ru.kpfu.itis.app.security.role.Role;
import ru.kpfu.itis.app.services.UserService;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Maxim Yagafarov
 **/
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private JobAppRepository jobAppRepository;

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ParticipantRepository participantRepository;

    @Override
    public Optional<User> findOneByLogin(String login) {
        return usersRepository.findOneByLogin(login);
    }


    @Override
    public void updateInfo(ManagerEditForm managerEditForm, User user) {
        user.setLogin(managerEditForm.getLogin());
        user.setEmail(managerEditForm.getEmail());
        usersRepository.save(user);
    }

    @Override
    @Transactional
    public void deleteUser(Long id) {
        Optional<User> userOpt = usersRepository.findOneById(id);
        if (!userOpt.isPresent()) return;
        User user = userOpt.get();
        if (user.getRole().equals(Role.PARTICIPANT) ||
                user.getRole().equals(Role.POTENTIAL_PARTICIPANT)) {
            ParticipantData part = participantRepository.findOneByUser(user).get();
            List<Conversation> conv = conversationRepository.findAllByUser1(part);
            System.out.println("conv i want to delete = " + conv);
            conversationRepository.delete(conv);
            conversationRepository.delete(conversationRepository.findAllByUser2(part));
            questionRepository.delete(questionRepository.findAllByAuthor(part));
            jobAppRepository.delete(jobAppRepository.findAllByAuthor(part));
            participantRepository.delete(part);
        } else {
            questionRepository.save(
                    questionRepository.findAllByManager(user).stream()
                            .filter((q) -> q.getManager().equals(user)).
                            map((q) -> {
                                q.setManager(null);
                                return q;
                    }).collect(Collectors.toList()));
            usersRepository.delete(user);
        }
    }

    @Override
    public Optional<User> updateInfo(UserEditForm userEditForm) {
        Optional<User> userOpt = usersRepository.findOneById(userEditForm.getId());
        if (!userOpt.isPresent()) return userOpt;
        User user = userOpt.get();
        user.setEmail(userEditForm.getEmail());
        user.setLogin(userEditForm.getLogin());
//        user.setRole(Role.getByText(userEditForm.getRole()));
        return Optional.of(usersRepository.save(user));
    }

    @Override
    public User create(UserCreateForm userCreateForm) {
        User user = User.builder()
                .login(userCreateForm.getLogin())
                .email(userCreateForm.getEmail())
                .hashPassword(passwordEncoder.encode(userCreateForm.getPassword()))
                .role(Role.getByText(userCreateForm.getRole()))
                .build();
        return usersRepository.save(user);
    }
}
