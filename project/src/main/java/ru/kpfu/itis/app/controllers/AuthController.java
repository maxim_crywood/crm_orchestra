package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.app.forms.InstrumentCreateForm;
import ru.kpfu.itis.app.forms.UserRegistrationForm;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.security.role.Role;
import ru.kpfu.itis.app.services.AuthenticationService;
import ru.kpfu.itis.app.services.InstrumentService;
import ru.kpfu.itis.app.services.RegistrationService;
import ru.kpfu.itis.app.services.UserService;
import ru.kpfu.itis.app.validators.UserRegistrationFormValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
@Controller
public class AuthController {

    @Autowired
    private AuthenticationService service;

    @Autowired
    private UserRegistrationFormValidator userRegistrationFormValidator;

    @Autowired
    private UserService userService;


    @InitBinder("userRegistrationForm")
    public void initUserRegistrationFormValidator(WebDataBinder binder) {
        binder.addValidators(userRegistrationFormValidator);
    }


    @GetMapping("/login")
    public String login(@ModelAttribute("model") ModelMap model, Authentication authentication,
                        @RequestParam Optional<String> error) {
        if (authentication != null) {
            return "redirect:/";
        }
        model.addAttribute("error", error);
        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, Authentication authentication) {
        if (authentication != null) {
            request.getSession().invalidate();
        }
        return "redirect:/login";
    }

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UsersRepository usersRepository;


    @PostMapping("/check")
    @ResponseBody
    public String checkLogin(@RequestParam("login") String login){
        if (userService.findOneByLogin(login).isPresent()){
            return "true";
        }else{
            return "false";
        }
    }

    @GetMapping("/")
    public String root(Authentication authentication) {
        if (authentication != null) {
            User user = service.getUserByAuthentication(authentication);

            if (user.getRole().equals(Role.ADMIN)) {
                return "redirect:/admin/users";
            } else if (user.getRole().equals(Role.MANAGER)) {
                return "redirect:/manager/profile";
            } else {
                return "redirect:/participant/profile";
            }
        }
        return "redirect:/login";
    }

    @GetMapping("/profile")
    public String getProfilePage(Authentication authentication, @ModelAttribute("model") ModelMap model) {
        User user = service.getUserByAuthentication(authentication);
        if (user.getRole() == Role.ADMIN) {
            return "redirect:/admin";
        } else if (user.getRole() == Role.MANAGER) {
            return "redirect:/manager/profile";
        } else {
            return "redirect:/participant/profile";
        }
    }


    @Autowired
    private RegistrationService reg;

    @Autowired
    private InstrumentService instrumentService;

    @GetMapping("/keklink")
    public String keklink() {
        User user = User.builder()
                .login("admin")
                .email("admin@gmail.com")
                .role(Role.ADMIN)
                .hashPassword(passwordEncoder.encode("admin"))
                .build();
        usersRepository.save(user);
        reg.register(UserRegistrationForm.builder()
                .email("max@gmail.com")
                .login("max")
                .password("max")
                .role(Role.MANAGER.getText())
                .build());
        ;
        reg.register(UserRegistrationForm.builder()
                .email("nikita@gmail.com")
                .login("nikita")
                .password("nikita")
                .role(Role.POTENTIAL_PARTICIPANT.getText())
                .build());

        reg.register(UserRegistrationForm.builder()
                .email("vlad@gmail.com")
                .login("vladimir")
                .password("vladimir")
                .role(Role.MANAGER.getText())
                .build());

        reg.register(UserRegistrationForm.builder()
                .email("timur@gmail.com")
                .login("timur")
                .password("timur")
                .role(Role.POTENTIAL_PARTICIPANT.getText())
                .build());

        instrumentService.create(
                InstrumentCreateForm.builder()
                    .description("trumpet big")
                    .name("trumpet")
                    .build());

        instrumentService.create(
                InstrumentCreateForm.builder()
                        .description("guitar electro")
                        .name("electric guitar")
                        .build());

        instrumentService.create(
                InstrumentCreateForm.builder()
                        .description("classic guitar")
                        .name("classic quitar")
                        .build());

        instrumentService.create(
                InstrumentCreateForm.builder()
                        .description("piano")
                        .name("piano")
                        .build());

        instrumentService.create(
                InstrumentCreateForm.builder()
                        .description("drums")
                        .name("drums")
                        .build());

        instrumentService.create(
                InstrumentCreateForm.builder()
                        .description("bass guitar")
                        .name("bass guitar")
                        .build());

        instrumentService.create(
                InstrumentCreateForm.builder()
                        .description("violin")
                        .name("violin")
                        .build());

        instrumentService.create(
                InstrumentCreateForm.builder()
                        .description("cello")
                        .name("cello")
                        .build());

        return "redirect:/";
    }

}
