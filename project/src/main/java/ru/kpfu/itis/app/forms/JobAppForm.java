package ru.kpfu.itis.app.forms;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Maxim Yagafarov
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class JobAppForm {
    private Long wantedSalary;
    private String instrumentName;
    private String description;
    private String email;
    private MultipartFile file;
}
