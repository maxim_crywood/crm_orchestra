package ru.kpfu.itis.app.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Maxim Yagafarov
 **/
public enum  JobAppStatus {
    IN_PROGRESS("В прогрессе"),
    INTERVIEW("Собеседование"),
    APPROVED("Заявка принята"),
    DENIED("Заявка отклонена");

    private String text;
    public static Map<String, JobAppStatus> all;


    private JobAppStatus(String text) {
        this.text = text;
    }

    public static JobAppStatus getByText(String text) {
        if (all == null){
            all = new HashMap<>();
            for (JobAppStatus status : JobAppStatus.values()) {
                all.put(status.text, status);
            }
        }
        return all.get(text);
    }

    @Override
    public String toString() {
        return text;
    }

    public String getText() {
        return text;
    }

}
