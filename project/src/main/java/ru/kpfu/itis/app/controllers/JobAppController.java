package ru.kpfu.itis.app.controllers;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.app.forms.JobAppForm;
import ru.kpfu.itis.app.model.*;
import ru.kpfu.itis.app.security.role.Role;
import ru.kpfu.itis.app.services.AuthenticationService;
import ru.kpfu.itis.app.services.DocumentService;
import ru.kpfu.itis.app.services.JobAppService;
import ru.kpfu.itis.app.services.ParticipantService;
import ru.kpfu.itis.app.validators.JobAppFormValidator;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/

@Controller
public class JobAppController {


    @Autowired
    private DocumentService documentService;

    @Autowired
    private JobAppService jobAppService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private JobAppFormValidator jobAppFormValidator;

    @Autowired
    private UserHelper userHelper;

    @InitBinder("jobAppForm")
    public void initJobAppForm(WebDataBinder binder) {
        binder.addValidators(jobAppFormValidator);
    }

    @GetMapping("/participant/jobapp")
    public String makeJobApp(@ModelAttribute("model") ModelMap model,
                             Authentication authentication) {
        userHelper.prepareParticipantView(model, authentication);
        model.addAttribute("instruments",
                participantService.getUserByAuthentication(authentication).get().getInstruments());
        return "jobapp";
    }

    @GetMapping("/participant/all_jobapps")
    public String showJobApps(@ModelAttribute("model") ModelMap model,
                              Authentication authentication) {
        userHelper.prepareParticipantView(model, authentication);
        ParticipantData participant = participantService.getUserByAuthentication(authentication).get();
        List<JobApp> jobApps = jobAppService.findAllByAuthor(participant);
        model.addAttribute("jobapps", jobApps);
        return "participant_all_jobapps";
    }

    @PostMapping("/participant/add_jobapp")
    public String addJobApp(@Valid @ModelAttribute("jobAppForm")JobAppForm jobAppForm,
                            BindingResult result,
                            Authentication authentication,
                            RedirectAttributes attributes) {
        if (result.hasErrors()) {
            System.out.println("result.getAllErrors() = " + result.getAllErrors());
            return "redirect:/participant/jobapp";
        }
        JobApp jobApp = jobAppService.createJobApp(jobAppForm, participantService.getUserByAuthentication(authentication).get());
        attributes.addAttribute("id", jobApp.getId());
        return "redirect:/participant/concrete_jobapp";
    }

    @GetMapping("/download_cv")
    @SneakyThrows
    public String getCv(
            @RequestParam Long jobappId,
            @RequestParam Long id,
            HttpServletResponse response,
            Authentication authentication) {
        Optional<Document> documentOpt = documentService.findById(id);
        if (!documentOpt.isPresent())
            return "redirect:/";
        Document document = documentOpt.get();
        User user = authenticationService.getUserByAuthentication(authentication);
        if (user.getRole().equals(Role.PARTICIPANT) ||
                user.getRole().equals(Role.POTENTIAL_PARTICIPANT)) {
            ParticipantData participant = participantService.getDataByUser(user).get();
            Optional<JobApp> jobAppOpt = jobAppService.findOneBy(jobappId);
            if (!jobAppOpt.isPresent()) {
                return "redirect:/";
            }
            JobApp jobApp = jobAppOpt.get();
            if (!jobApp.getAuthor().getId().equals(participant.getId())) {
                return "redirect:/";
            }
            if (!jobApp.getDocument().getId().equals(document.getId())) {
                return "redirect:/";
            }
        }


        response.setContentType("application/pdf");
        response.setContentLength(document.getContent().length);
        FileCopyUtils.copy(document.getContent(), response.getOutputStream());
        return "redirect:/";
    }

    @GetMapping("/participant/concrete_jobapp")
    public String showJobApp(@ModelAttribute("model") ModelMap model,
                             @RequestParam Long id,
                             Authentication authentication) {
        userHelper.prepareParticipantView(model, authentication);
        JobApp jobApp = jobAppService.findOneBy(id).get();
        ParticipantData user = participantService.getUserByAuthentication(authentication).get();
        if (!jobApp.getAuthor().getId().equals(user.getId())) {
            return "redirect:/";
        }
        boolean sobes = jobApp.getStatus() == JobAppStatus.INTERVIEW;
        model.addAttribute("sobes", sobes);
        model.addAttribute("jobapp", jobApp);
        return "participant_concrete_jobapp";

    }

}
