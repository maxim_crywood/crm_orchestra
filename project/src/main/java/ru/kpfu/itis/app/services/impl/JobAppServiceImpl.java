package ru.kpfu.itis.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.app.forms.JobAppForm;
import ru.kpfu.itis.app.model.Document;
import ru.kpfu.itis.app.model.JobApp;
import ru.kpfu.itis.app.model.JobAppStatus;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.repositories.InstrumentRepository;
import ru.kpfu.itis.app.repositories.JobAppRepository;
import ru.kpfu.itis.app.services.DocumentService;
import ru.kpfu.itis.app.services.JobAppService;

import java.util.*;

/**
 * Maxim Yagafarov
 **/
@Service
public class JobAppServiceImpl implements JobAppService {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private JobAppRepository jobAppRepository;

    @Autowired
    private InstrumentRepository instrumentRepository;

    @Override
    @Transactional
    public JobApp createJobApp(JobAppForm form, ParticipantData author) {
        Document document = documentService.saveDocument(form.getFile());
        JobApp jobApp = JobApp.builder()
                .author(author)
                .date(new Date(System.currentTimeMillis()))
                .description(form.getDescription())
                .instrument(instrumentRepository.findOneByName(form.getInstrumentName()).get())
                .status(JobAppStatus.IN_PROGRESS)
                .wantedSalary(Math.toIntExact(form.getWantedSalary()))
                .document(document)
                .email(form.getEmail())
                .build();
        return jobAppRepository.save(jobApp);
    }

    @Override
    @Transactional
    public Optional<JobApp> findOneBy(Long id) {
        return jobAppRepository.findOneById(id);
    }

    @Override
    @Transactional
    public List<JobApp> findAllByAuthor(ParticipantData author) {
        List<JobApp> ret = jobAppRepository.findAllByAuthor(author);
        sortByDate(ret, true);
        return ret;
    }

    private void sortByDate(List<JobApp> ret, boolean inv) {
        Collections.sort(ret, Comparator.comparing(JobApp::getDate));
        if (inv)
            Collections.reverse(ret);
    }

    @Override
    @Transactional
    public List<JobApp> findAllByStatus(JobAppStatus status) {
        List<JobApp> ret = jobAppRepository.findAllByStatus(status);
        sortByDate(ret, true);
        return ret;
    }

    @Override
    public List<JobApp> findAll() {
        List<JobApp> ret = jobAppRepository.findAll();
        sortByDate(ret, true);
        return ret;
    }

    @Override
    public void changeStatus(JobApp jobApp, JobAppStatus status) {
        jobApp.setStatus(status);
        jobAppRepository.save(jobApp);
    }
}
