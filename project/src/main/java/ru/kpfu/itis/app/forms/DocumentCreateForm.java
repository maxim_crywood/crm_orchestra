package ru.kpfu.itis.app.forms;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Maxim Yagafarov
 **/

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DocumentCreateForm {
    private MultipartFile file;
}