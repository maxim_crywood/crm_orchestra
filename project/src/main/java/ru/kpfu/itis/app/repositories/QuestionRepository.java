package ru.kpfu.itis.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.app.model.ParticipantData;
import ru.kpfu.itis.app.model.Question;
import ru.kpfu.itis.app.model.User;

import java.util.List;
import java.util.Optional;

/**
 * Maxim Yagafarov
 **/
public interface QuestionRepository extends JpaRepository<Question, Long> {
    Optional<Question> findOneById(Long id);
    List<Question> findAllByAuthor(ParticipantData author);
    List<Question> findAll();

    List<Question> findAllByManager(User user);
}
