package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.app.model.Question;
import ru.kpfu.itis.app.services.AuthenticationService;
import ru.kpfu.itis.app.services.ParticipantService;
import ru.kpfu.itis.app.services.QuestionService;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Maxim Yagafarov
 **/

@Controller
public class QuestionController {

    @Autowired
    private UserHelper userHelper;

    @Autowired
    private QuestionService service;

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private AuthenticationService authenticationService;

    @GetMapping("/participant/question")
    public String showQuestion(@ModelAttribute("model") ModelMap model,
                               Authentication authentication) {
        userHelper.prepareParticipantView(model, authentication);

        return "question";
    }

    @PostMapping("/participant/ask_question")
    public String askQuestion(@ModelAttribute("model") ModelMap model,
                              @RequestParam String content,
                              RedirectAttributes attributes,
                              Authentication authentication) {
        System.out.println("asking");
        Question question =
                service.addQuestion(content, participantService.getUserByAuthentication(authentication).get());

        attributes.addAttribute("id", question.getId());
        return "redirect:/participant/concrete_question";
    }


    @PostMapping("/manager/answer")
    public String answer(@ModelAttribute("model") ModelMap model,
                         @RequestParam String answer,
                         @RequestParam Long id,
                         Authentication authentication,
                         RedirectAttributes attributes) {
        Optional<Question> opt = service.findOneById(id);
        if (!opt.isPresent()) return "redirect:/";
        Question question = opt.get();
        question = service.answer(question, answer, authenticationService.getUserByAuthentication(authentication));
        attributes.addAttribute("id", id);
        return "redirect:/manager/concrete_question";
    }

    @GetMapping("/participant/concrete_question")
    public String showConcreteQuestion(@ModelAttribute("model") ModelMap model,
                               @RequestParam Long id,
                               Authentication authentication) {
        System.out.println("In concrete question");
        userHelper.prepareParticipantView(model, authentication);

        Optional<Question> opt = service.findOneById(id);
        if (!opt.isPresent()) {
            return "redirect:/participant/all_questions";
        }
        model.addAttribute("question", opt.get());
        if (opt.get().getAnswered())
            model.addAttribute("rotebal",true);
        else
            model.addAttribute("rotebal",false);
        return "participant_concrete_question";
    }

    @GetMapping("/manager/concrete_question")
    public String showConcreteQuestionKek(@ModelAttribute("model") ModelMap model,
                                       @RequestParam Long id,
                                       Authentication authentication) {
        userHelper.prepareParticipantView(model, authentication);

        Optional<Question> opt = service.findOneById(id);
        if (!opt.isPresent()) {
            return "redirect:/manager/all_questions";
        }
        model.addAttribute("question", opt.get());
        if (opt.get().getAnswered())
            model.addAttribute("rotebal",true);
        else
            model.addAttribute("rotebal",false);
        return "manager_concrete_question";
    }


    @GetMapping("/participant/all_questions")
    public String getAllQuestionsByParticipant(@ModelAttribute("model") ModelMap model,
                               Authentication authentication) {
        userHelper.prepareParticipantView(model, authentication);
        model.addAttribute("questions", service.findAllByAuthor(
                participantService.getUserByAuthentication(authentication).get()
        ));
        return "participant_all_questions";
    }

    @GetMapping("/manager/all_questions")
    public String getAllQuestions(@ModelAttribute("model") ModelMap model,
                                  Authentication authentication) {
        userHelper.prepareParticipantView(model, authentication);
        model.addAttribute("questions", service.findAll()
                .stream()
                .filter((q) -> q.getAnswer() == null)
                .collect(Collectors.toList())
        );
        return "manager_all_questions";
    }
}
