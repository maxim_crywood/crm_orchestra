package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.app.forms.InstrumentCreateForm;
import ru.kpfu.itis.app.forms.InstrumentEditForm;
import ru.kpfu.itis.app.forms.UserCreateForm;
import ru.kpfu.itis.app.forms.UserEditForm;
import ru.kpfu.itis.app.model.Instrument;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.repositories.InstrumentRepository;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.security.role.Role;
import ru.kpfu.itis.app.services.InstrumentService;
import ru.kpfu.itis.app.services.ParticipantService;
import ru.kpfu.itis.app.services.UserService;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Maxim Yagafarov
 **/

@Controller
public class AdminInstrumentCRUDController {

    @Autowired
    private InstrumentService instrumentService;

    @Autowired
    private InstrumentRepository instrumentRepository;


    @GetMapping("admin/instruments")
    public String getInstruments(@ModelAttribute("model")ModelMap model) {
        model.addAttribute("instruments", instrumentRepository.findAll());
        return "admin_instruments";
    }

    @GetMapping("/admin/instrument")
    public String getUser(@ModelAttribute("model") ModelMap model,
                          @RequestParam Long id) {

        model.addAttribute("instrument", instrumentRepository.findOneById(id).get());
        return "admin_instrument";
    }

    @PostMapping("/admin/instrument/delete")
    public String deleteUser(@RequestParam Long id) {
        instrumentService.deleteInstrument(id);
        return "redirect:/admin/instruments";
    }

    @PostMapping("admin/instrument/edit")
    public String editUser(@ModelAttribute("instrumentEditForm")InstrumentEditForm instrumentEditForm,
                           RedirectAttributes attributes) {
        instrumentService.updateInfo(instrumentEditForm);
        attributes.addAttribute("id", instrumentEditForm.getId());
        return "redirect:/admin/instrument";
    }

    @GetMapping("/admin/instrument/creation")
    public String getCreationPage(@ModelAttribute("model") ModelMap model) {
        return "admin_instrument_creation";
    }

    @PostMapping("/admin/instrument/create")
    public String create(@ModelAttribute("instrumentCreateForm") InstrumentCreateForm instrumentCreateForm,
                         RedirectAttributes attributes) {
        Instrument instrument = instrumentService.create(instrumentCreateForm);
        attributes.addAttribute("id", instrument.getId());
        return "redirect:/admin/instrument";
    }
}
