package ru.kpfu.itis.app.security.status;

/**
 * Maxim Yagafarov
 **/

public enum UserStatus {
    CONFIRMED, UNCONFIRMED, BANNED
}
